import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LectureListPage } from './lecture-list.page';

describe('LectureListPage', () => {
  let component: LectureListPage;
  let fixture: ComponentFixture<LectureListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LectureListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LectureListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
