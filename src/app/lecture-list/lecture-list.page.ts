import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';

import { LecturePage } from '../lecture/lecture.page';
import { InfoPopupPage } from '../info-popup/info-popup.page';
import { ExamPage } from '../exam/exam.page';

@Component({
  selector: 'app-lecture-list',
  templateUrl: './lecture-list.page.html',
  styleUrls: ['./lecture-list.page.scss'],
})
export class LectureListPage implements OnInit {

  constructor(public modalcontroller: ModalController, public popoverController: PopoverController) { }

  ngOnInit() {
  }

  async infoPopup(ev: any) {
    const popover = await this.popoverController.create({
      component: InfoPopupPage,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  async openLecture() {
    const modal =  await this.modalcontroller.create({
      component: LecturePage,
      componentProps: { value: 0}
    });
    return await modal.present();
  }

  // async openExam() {
  //   const modal =  await this.modalcontroller.create({
  //     component: ExamPage,
  //     componentProps: { value: 0}
  //   });
  //   return await modal.present();
  // }

}
