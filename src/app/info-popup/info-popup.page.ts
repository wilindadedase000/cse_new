import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { AccountSettingsPage } from '../account-settings/account-settings.page';
import { AboutPage } from '../about/about.page';

@Component({
  selector: 'app-info-popup',
  templateUrl: './info-popup.page.html',
  styleUrls: ['./info-popup.page.scss'],
})
export class InfoPopupPage implements OnInit {

  constructor(public modalcontroller: ModalController,public popoverController: PopoverController) { }

  ngOnInit() {
  }
  
  close() {
    this.popoverController.dismiss();
  }

  async accountsettings() {
    this.popoverController.dismiss();
    const modal =  await this.modalcontroller.create({
      component: AccountSettingsPage,
      componentProps: { value: 0}
    });
    return await modal.present();
  }

  async about() {
    this.popoverController.dismiss();
    const modal =  await this.modalcontroller.create({
      component: AboutPage,
      componentProps: { value: 0}
    });
    return await modal.present();
  }
  
  
  rateUs(){
    window.location.href = 'https://play.google.com/store/apps/details?id=com.roadeo.techmates'
  }

}
