import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoPopupPage } from './info-popup.page';

describe('InfoPopupPage', () => {
  let component: InfoPopupPage;
  let fixture: ComponentFixture<InfoPopupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoPopupPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoPopupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
