import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LectureExamPage } from './lecture-exam.page';

describe('LectureExamPage', () => {
  let component: LectureExamPage;
  let fixture: ComponentFixture<LectureExamPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LectureExamPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LectureExamPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
