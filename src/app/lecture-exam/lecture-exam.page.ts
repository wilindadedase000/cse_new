import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

import { InfoPopupPage } from '../info-popup/info-popup.page';

@Component({
  selector: 'app-lecture-exam',
  templateUrl: './lecture-exam.page.html',
  styleUrls: ['./lecture-exam.page.scss'],
})
export class LectureExamPage implements OnInit {

  constructor(private popoverController:PopoverController) { }

  ngOnInit() {
  }

  async infoPopup(ev: any) {
    const popover = await this.popoverController.create({
      component: InfoPopupPage,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

}
