import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'home', loadChildren: './home/home.module#HomePageModule'},
  { path: 'list', loadChildren: './list/list.module#ListPageModule'},
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'lecture', loadChildren: './lecture/lecture.module#LecturePageModule' },
  { path: 'exam', loadChildren: './exam/exam.module#ExamPageModule' },
  { path: 'instruction', loadChildren: './instruction/instruction.module#InstructionPageModule' },
  { path: 'account-settings', loadChildren: './account-settings/account-settings.module#AccountSettingsPageModule' },
  { path: 'final-score', loadChildren: './final-score/final-score.module#FinalScorePageModule' },
  { path: 'review', loadChildren: './review/review.module#ReviewPageModule' },
  { path: 'about', loadChildren: './about/about.module#AboutPageModule' },
  { path: 'lecture-list', loadChildren: './lecture-list/lecture-list.module#LectureListPageModule' }
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
