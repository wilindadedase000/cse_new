import { Component } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ScrollDetail } from '@ionic/core';

import { InfoPopupPage } from '../info-popup/info-popup.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  showToolbar: boolean = true;

  constructor(private popoverController:PopoverController) {}

  async infoPopup(ev: any) {
    const popover = await this.popoverController.create({
      component: InfoPopupPage,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  onScroll($event: CustomEvent<ScrollDetail>){
    if($event && $event.detail && $event.detail.scrollTop){
      const scrollTop = $event.detail.scrollTop;
      this.showToolbar = false;
      this.showToolbar = scrollTop >= 50;
    }
  }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 1000);
  }

}
