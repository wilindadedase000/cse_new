import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ScrollDetail } from '@ionic/core';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.page.html',
  styleUrls: ['./account-settings.page.scss'],
})
export class AccountSettingsPage implements OnInit {
  showToolbar: boolean = true;

  constructor(public modalcontroller: ModalController) { }

  ngOnInit() {
  }

  dismiss(){
    this.modalcontroller.dismiss();
  }
  
  onScroll($event: CustomEvent<ScrollDetail>){
    if($event && $event.detail && $event.detail.scrollTop){
      const scrollTop = $event.detail.scrollTop;
      this.showToolbar = false;
      this.showToolbar = scrollTop >= 50;
    }
  }

}
