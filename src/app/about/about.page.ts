import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ScrollDetail } from '@ionic/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
  showToolbar: boolean = true;

  constructor(public modalcontroller: ModalController) { }

  ngOnInit() {
  }

  dismiss(){
    this.modalcontroller.dismiss();
  }

  onScroll($event: CustomEvent<ScrollDetail>){
    if($event && $event.detail && $event.detail.scrollTop){
      const scrollTop = $event.detail.scrollTop;
      this.showToolbar = false;
      this.showToolbar = scrollTop >= 50;
    }
  }


}
