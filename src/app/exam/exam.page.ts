import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

import { InstructionPage } from '../instruction/instruction.page';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.page.html',
  styleUrls: ['./exam.page.scss'],
})
export class ExamPage implements OnInit {

  constructor(private popoverController:PopoverController,public modalcontroller: ModalController) { }

  ngOnInit() {
    this.instruction("any");
  }

  async instruction(ev: any) {
    const popover = await this.popoverController.create({
      component: InstructionPage,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

}
