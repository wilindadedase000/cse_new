import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-instruction',
  templateUrl: './instruction.page.html',
  styleUrls: ['./instruction.page.scss'],
})
export class InstructionPage implements OnInit {

  constructor(private popoverController:PopoverController,public modalcontroller: ModalController, private router: Router) { }

  ngOnInit() {
  }

  start(){
    this.popoverController.dismiss(); 
  }

  notnow(){
    this.popoverController.dismiss();
    this.router.navigate(["/home"]); 
  }

}
