import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HomePageModule } from './home/home.module';
import { LectureListPageModule } from './lecture-list/lecture-list.module';
import { ExamPageModule } from './exam/exam.module';
import { InfoPopupPageModule } from './info-popup/info-popup.module';
import { AccountSettingsPageModule } from './account-settings/account-settings.module';
import { AboutPageModule } from './about/about.module';
import { LecturePageModule } from './lecture/lecture.module';
import { InstructionPageModule } from './instruction/instruction.module';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HomePageModule,
    LecturePageModule,
    ExamPageModule,
    InfoPopupPageModule,
    AccountSettingsPageModule,
    AboutPageModule,
    LectureListPageModule,
    InstructionPageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
