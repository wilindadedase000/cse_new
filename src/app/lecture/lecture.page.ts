import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

import { InfoPopupPage } from '../info-popup/info-popup.page';

@Component({
  selector: 'app-lecture',
  templateUrl: './lecture.page.html',
  styleUrls: ['./lecture.page.scss'],
})
export class LecturePage implements OnInit {

  constructor(private popoverController:PopoverController, public modalcontroller: ModalController) { }

  ngOnInit() {
  }

  async infoPopup(ev: any) {
    const popover = await this.popoverController.create({
      component: InfoPopupPage,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  // segmentChanged(ev: any) {
  //   console.log('Segment changed', ev);
  // }

  dismiss(){
    this.modalcontroller.dismiss();
  }


}
